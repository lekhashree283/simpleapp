# Simple-App
## ThoughtVault

ThoughtVault is a personal journaling standalone application built using python that provides a secure and reflective space for users to record their thoughts, experiences, and emotions. Whether you're looking to express yourself, set goals, or simply capture the moments of your life.

# Start the ThoughtVault application
python thought-vault.py

## Gitlab CI/CD setup

Create a `.gitlab-ci.yml` with three stages
- build
- test
- deploy

Mention the image and the script
`image: python:${PYTHON_VERSION}`
`script:`
    `- python <filename.py>`

Push code to check the CI/CD pipeline

## Gitlab CI/CD output
![My Image](img1.png)

## Jenkins pipeline setup

 - Create a `Jenkinsfile` with four stages
    - checkout
    - build
    - test
    - deploy

- Confirm the Python script path in your Jenkinsfile is accurate.
- Open Jenkins and create a new pipeline.
- Choose `Pipeline script from SCM` and enter your GitLab repository link.
- Select the branch (e.g., main) where your code resides.
- Specify the Jenkinsfile name from your GitLab repo.
- Save the configuration.
- Click "Build Now" to execute the pipeline.
- Check the console output for any errors or issues during execution.

## Jenkins pipeline Output
![My Image](img2.png)







